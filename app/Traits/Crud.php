<?php

namespace App\Traits;

trait Crud
{
    public function scopeIndex($query){
        return $query->withTrashed()->get();
    }

    public function scopeStore($query, $request){
        return $query->Create($request);
    }

    public function scopeShow($query, $id){
        $exists = $query->whereId($id)->exists();
        if($exists){
            return $query->whereId($id)->first();
        }else{
            return $this->error();
        }
    }

    public function scopeModernize($query, $request){
        $exists = $query->whereId($request['id'])->exists();
        if($exists){
            return $query->whereId($request['id'])->update($request);
        }else{
            return $this->error();
        }
    }

    public function scopeSoftDelete($query, $request){
        $exists = $query->withTrashed()->whereId($request['id'])->exists();
        if($exists){
            if(isset($query->whereId($request['id'])->first()->deleted_at)){
                return $query->whereId($request['id'])->restore();
            }else{
                return $query->whereId($request['id'])->delete();
            }
        }else{
            return $this->error();
        }
    }

    public function error(){
        $error = json_encode([
            'titulo' => 'error',
            'mensaje'=> 'No existe'
        ]);
        return response($error, 404)->header('Content-Type', 'application/json');
    } 
}
