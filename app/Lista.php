<?php

namespace App;

use App\Traits\Crud;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes; 

class Lista extends Model
{
    use SoftDeletes;
    use Crud;

    protected $fillable=[
        'nombre'
    ];

    public function setNombreAttribute($value)
    {
        $this->attributes['nombre'] = ucwords($value);
    }

    public function getNombreAttribute($value)
    {
        return ucwords($value);
    }
}
