<?php

namespace App\Http\Controllers;

use App\Lista;
use Illuminate\Http\Request;

class ListaController extends Controller
{
    public function index()
    {
        return Lista::index();
    }
    public function store(Request $request)
    {
        return Lista::store($request->all());
    }
    public function show($id)
    {
        return Lista::show($id);
    }
    public function update(Request $request)
    {
        return Lista::modernize($request->all());
    }
    public function destroy(Request $request)
    {
        return Lista::softdelete($request->all());
    }
}
